package com.neoxsam.zenlytest.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.neoxsam.zenlytest.R;
import com.neoxsam.zenlytest.adapter.ShareItemAdapter;
import com.neoxsam.zenlytest.adapter.ShareItemAdapter.ShareItem;
import com.neoxsam.zenlytest.service.AnimationService;
import com.neoxsam.zenlytest.service.AnimationService.AnimationListener;
import com.neoxsam.zenlytest.service.ServiceFactory;

import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsSharingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsSharingFragment extends Fragment implements AnimationListener {

    public final static int NB_MIL_IN_45_MINUTE = 2700000;
    public final static int NB_MIL_IN_15_MINUTE = 900000;

    private View mTimeLeftTitleTV;
    private TextView mTimeLeftTV;
    private View mExpendTimeTV;
    private View mStopTimeTV;
    private View mShareLinkTV;
    private boolean mIsAnimationRuning;

    private long mCurrentTimeRemaining = NB_MIL_IN_45_MINUTE;
    private CountDownTimer mCountDownTimer;

    public static SettingsSharingFragment newInstance() {
        SettingsSharingFragment fragment = new SettingsSharingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public SettingsSharingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings_sharing, null);
    }


    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        mTimeLeftTitleTV = view.findViewById(R.id.settings_text_view_time_left);
        mTimeLeftTV = (TextView) view.findViewById(R.id.text_view_timing);
        mExpendTimeTV = view.findViewById(R.id.card_view_extend_time);
        mStopTimeTV = view.findViewById(R.id.card_view_Stop);
        mShareLinkTV = view.findViewById(R.id.layout_sharing_link);

        ServiceFactory.getAnimationService().animateListOfViewAppear(
                mTimeLeftTitleTV, mTimeLeftTV,
                mExpendTimeTV, mStopTimeTV, mShareLinkTV
        );

        startNewTimmer();

        mExpendTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsAnimationRuning) {
                    mIsAnimationRuning = true;
                    ServiceFactory.getAnimationService().animateUpdateTextView(
                            mTimeLeftTV, SettingsSharingFragment.this
                    );
                }
            }
        });

        view.findViewById(R.id.icon_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.fragment_settings_share_dummy_link));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        mStopTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void startNewTimmer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }

        mCountDownTimer = new CountDownTimer(mCurrentTimeRemaining, 1000) {

            public void onTick(long millisUntilFinished) {
                mTimeLeftTV.setText(ServiceFactory.getAnimationService().getStringForMilliSec(millisUntilFinished, getActivity()));
                mCurrentTimeRemaining = millisUntilFinished;
            }

            public void onFinish() {
            }
        }.start();
    }

    //**********************************************************************
    // animation listener
    //**********************************************************************

    @Override
    public void onAnimationFirstPartDone() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        mCurrentTimeRemaining += NB_MIL_IN_15_MINUTE;
        startNewTimmer();
    }

    @Override
    public void onAnimationDone() {
        mIsAnimationRuning = false;
    }
}
