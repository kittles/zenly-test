package com.neoxsam.zenlytest.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neoxsam.zenlytest.R;
import com.neoxsam.zenlytest.adapter.ShareItemAdapter;
import com.neoxsam.zenlytest.adapter.ShareItemAdapter.ShareItem;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChooseSharingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseSharingFragment extends Fragment {

    public static ChooseSharingFragment newInstance() {
        ChooseSharingFragment fragment = new ChooseSharingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ChooseSharingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_sharing, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {

        view.findViewById(R.id.button_close_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.layout_sharing_link).setVisibility(View.GONE);
            }
        });

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_share_elem);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setAdapter(new ShareItemAdapter((OnSharingItemSelected) getActivity()));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);
        super.onViewCreated(view, savedInstanceState);
    }

    public interface OnSharingItemSelected {
        void onSharingItemSelected(ShareItem shareItem);
    }
}
