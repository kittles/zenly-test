package com.neoxsam.zenlytest.service;

/**
 * Created by neoxsam on 27/04/2016.
 */
public class ServiceFactory {

    private ServiceFactory() {
        //No need to instantiate the Factory
    }

    public static AnimationService getAnimationService(){
        return new AnimationService();
    }

}
