package com.neoxsam.zenlytest.service;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

/**
 * Created by neoxsam on 27/04/2016.
 */
public class AnimationService {

    AnimationService() {
        //This class can only be instantiated by the Factory
    }

    public void animateListOfViewAppear(View... viewList) {
        if (viewList != null && viewList.length > 0) {
            int startDelay = 0;
            for (View view : viewList) {
                animateViewAppear(view, startDelay);
                startDelay += 100;
            }
        }
    }

    public void animateUpdateTextView(final TextView textView, final AnimationListener listener) {

        int animTime = 300;

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(textView, "alpha", 1.0f, 0.0f).setDuration(animTime);
        ObjectAnimator animScaleOutX = ObjectAnimator.ofFloat(textView, "scaleX", 1.0f, 0.0f).setDuration(animTime);
        ObjectAnimator animScaleOutY = ObjectAnimator.ofFloat(textView, "scaleY", 1.0f, 0.0f).setDuration(animTime);

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(textView, "alpha", 0.0f, 1.0f).setDuration(animTime);
        ObjectAnimator animScaleInX = ObjectAnimator.ofFloat(textView, "scaleX", 0.0f, 1.0f).setDuration(animTime);
        animScaleInX.setInterpolator(new OvershootInterpolator());
        ObjectAnimator animScaleInY = ObjectAnimator.ofFloat(textView, "scaleY", 0.0f, 1.0f).setDuration(animTime);
        animScaleInY.setInterpolator(new OvershootInterpolator());

        fadeIn.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (listener != null) {
                    listener.onAnimationFirstPartDone();
                }
                super.onAnimationStart(animation);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null) {
                    listener.onAnimationDone();
                }
                super.onAnimationEnd(animation);
            }
        });

        animatorSet.play(fadeOut).with(animScaleOutX).with(animScaleOutY).before(fadeIn).before(animScaleInX).before(animScaleInY);
        animatorSet.start();
    }

    public void animateViewAppear(final View view, int startDelay) {
        if (view == null) {
            return;
        }

        view.setAlpha(0);
        view.setScaleX(0);
        view.setScaleY(0);
        view.setTranslationY(view.getY() + 200);
        view.animate().alpha(1)
                .translationY(0)
                .scaleX(1)
                .scaleY(1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                        super.onAnimationStart(animation);
                    }
                }).setDuration(700).setStartDelay(startDelay).setInterpolator(new AccelerateDecelerateInterpolator()).start();

    }

    public String getStringForMilliSec(double milliSec, Context context) {
        int seconds = (int) (milliSec / 1000) % 60;
        int minutes = (int) ((milliSec / (1000 * 60)) % 60);
        int hours = (int) ((milliSec / (1000 * 60 * 60)) % 24);

        String timeRemaining = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        return timeRemaining;
    }

    public interface AnimationListener {
        void onAnimationFirstPartDone();
        void onAnimationDone();
    }
}
