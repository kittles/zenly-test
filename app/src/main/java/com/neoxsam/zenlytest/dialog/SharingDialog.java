package com.neoxsam.zenlytest.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.neoxsam.zenlytest.R;
import com.neoxsam.zenlytest.adapter.ShareItemAdapter.ShareItem;

/**
 * Created by neoxsam on 27/04/2016.
 */
public class SharingDialog extends DialogFragment {

    private static final String EXTRA_SHARE_ITEM = "extra_share_item";

    private AlertDialog mAlertDialog;
    private View mDialogView;
    private ShareItem mShareItem;

    public static DialogFragment newInstance(ShareItem shareItem) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_SHARE_ITEM, shareItem);

        SharingDialog sharingDialog = new SharingDialog();
        sharingDialog.setArguments(bundle);

        return sharingDialog;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {

        mShareItem = (ShareItem) getArguments().getSerializable(EXTRA_SHARE_ITEM);

        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mDialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_sharing, null);

        mAlertDialog = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle)
                .setTitle(mShareItem.getTitleResId())
                .setView(mDialogView)
                .setPositiveButton("Share !", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (getActivity() instanceof OnShareDialogSelected) {
                            ((OnShareDialogSelected) getActivity()).onShareDialogSelected();
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();

        return mAlertDialog;
    }

    public interface OnShareDialogSelected {
        void onShareDialogSelected();
    }
}
