package com.neoxsam.zenlytest.activity;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.neoxsam.zenlytest.R;
import com.neoxsam.zenlytest.adapter.ShareItemAdapter;
import com.neoxsam.zenlytest.dialog.SharingDialog;
import com.neoxsam.zenlytest.dialog.SharingDialog.OnShareDialogSelected;
import com.neoxsam.zenlytest.fragment.ChooseSharingFragment;
import com.neoxsam.zenlytest.fragment.ChooseSharingFragment.OnSharingItemSelected;
import com.neoxsam.zenlytest.fragment.SettingsSharingFragment;

public class ShareActivity extends AppCompatActivity implements OnSharingItemSelected, OnShareDialogSelected {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        replaceFragment(ChooseSharingFragment.newInstance());
    }

    private void replaceFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container_body, fragment, Fragment.class.getSimpleName())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    //************************************************************
    // Sharing item interface
    //************************************************************

    @Override
    public void onSharingItemSelected(ShareItemAdapter.ShareItem shareItem) {
        DialogFragment dialog = SharingDialog.newInstance(shareItem);
        dialog.show(getFragmentManager(), dialog.getClass().getSimpleName());
    }

    @Override
    public void onShareDialogSelected() {
        replaceFragment(SettingsSharingFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }
}
