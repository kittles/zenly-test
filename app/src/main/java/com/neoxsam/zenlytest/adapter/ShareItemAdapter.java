package com.neoxsam.zenlytest.adapter;

import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.neoxsam.zenlytest.R;
import com.neoxsam.zenlytest.fragment.ChooseSharingFragment;
import com.neoxsam.zenlytest.fragment.ChooseSharingFragment.OnSharingItemSelected;

/**
 * Created by neoxsam on 26/04/2016.
 */
public class ShareItemAdapter extends RecyclerView.Adapter<ShareItemAdapter.ViewHolder> {

    private ShareItem[] mShareItemList;
    private boolean mIsItemLoading;
    private OnSharingItemSelected mListener;

    public ShareItemAdapter(OnSharingItemSelected listener) {
        mShareItemList = ShareItem.values();
        mIsItemLoading = false;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_share, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ShareItem item = mShareItemList[position];

        holder.mTextViewShareName.setText(item.getTitleResId());

        holder.mCardViewBackGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsItemLoading) {
                    holder.mTextViewShareName.setVisibility(View.GONE);
                    holder.mProgressBar.setVisibility(View.VISIBLE);
                    mIsItemLoading = true;

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.mTextViewShareName.setVisibility(View.VISIBLE);
                            holder.mProgressBar.setVisibility(View.GONE);
                            mIsItemLoading = false;

                            if (mListener != null) {
                                mListener.onSharingItemSelected(item);
                            }
                        }
                    }, 3000);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mShareItemList.length;
    }

    public enum ShareItem {

        SHARE_SMS(R.drawable.icone_msg, R.string.sharing_message),
        SHARE_WHATSAPP(R.drawable.icone_whatsapp, R.string.sharing_whatsapp),
        SHARE_FACEBOOK(R.drawable.icone_fb, R.string.sharing_facebook),
        SHARE_TWITTER(R.drawable.icone_twitter, R.string.sharing_twitter),
        SHARE_EMAIL(R.drawable.icone_mail, R.string.sharing_email);

        private int mIconResId;
        private int mTitleResId;

        ShareItem(int iconResId, int titleResId) {
            mIconResId = iconResId;
            mTitleResId = titleResId;
        }

        public int getIconResId() {
            return mIconResId;
        }

        public int getTitleResId() {
            return mTitleResId;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mBodyView;
        public TextView mTextViewShareName;
        public CardView mCardViewBackGround;
        public ProgressBar mProgressBar;

        public ViewHolder(View view) {
            super(view);
            mTextViewShareName = (TextView) view.findViewById(R.id.text_view_share_name);
            mCardViewBackGround = (CardView) view.findViewById(R.id.card_view_background);
            mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar_loading);
            mBodyView = view;
        }
    }


}